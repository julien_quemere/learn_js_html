document.getElementById("fileInput").onchange = function(ev) {
  let f = this.files[0];
  let reader = new FileReader();

  reader.readAsText(f);

  reader.onload = function() {
    let p = document.createElement('P');
    p.innerText = reader.result;
    document.body.append(p);
  }

}

navigator.serviceWorker && navigator.serviceWorker.register('sw.js').then(function(registration) {  console.log('Excellent, registered with scope: ', registration.scope);});